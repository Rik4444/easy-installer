#!/bin/bash

JAVA_VERSION=$(cat $JAVA_VERSION_FILE | grep "appTitle" | sed -E 's/.*"(v.*)".*/\1/')
echo "Java version:      $JAVA_VERSION ($JAVA_VERSION_FILE)"

SNAPCRAFT_VERSION=$(cat $SNAPCRAFT_VERSION_FILE | grep "^version" | sed -E "s/^version: '(v.*)'.*/\1/")
echo "Snapcraft version: $SNAPCRAFT_VERSION ($SNAPCRAFT_VERSION_FILE)"

echo "Tag version:       $CI_COMMIT_TAG"

if [ $JAVA_VERSION != $CI_COMMIT_TAG ] || [ $SNAPCRAFT_VERSION != $CI_COMMIT_TAG ]
then
  exit 1
fi
