::!/bin/bash

:: Copyright (C) 2019 ECORP SAS - Author: Romain Hunault
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Exit status
:: - 0 : OEM unlocked on device
:: - 10 : heimdall error

heimdall print-pit

if %errorLevel==1 ( exit /b 10 )

timeout 5 >nul
