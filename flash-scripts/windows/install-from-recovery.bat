::!/bin/bash

:: Copyright (C) 2019 ECORP SAS - Author: Romain Hunault
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID device identifier
:: $2: ARCHIVE_PATH path to the /e/ archive to flash

:: Exit status
:: - 0 : /e/ installed
:: - 1 : Problems occurred (adb returns only 1 if an error occurs)
:: - 2 : Problems occurred during file transfer
:: - 3 : Problems occurred /e/ installation
:: - 101 : DEVICE_ID missing
:: - 102 : ARCHIVE_PATH missing

set "DEVICE_ID=%1"
set "ARCHIVE_PATH=%2"

for %%a in ("%ARCHIVE_PATH%") do (
	set "ARCHIVE_NAME=%%~na"
)

if not defined %DEVICE_ID (
  exit /b 101
)

if not defined %ARCHIVE_PATH (
  exit /b 102
)

adb -s %DEVICE_ID% shell twrp wipe system
adb -s %DEVICE_ID% shell twrp wipe cache
adb -s %DEVICE_ID% shell twrp wipe data
adb -s %DEVICE_ID% push  %ARCHIVE_PATH% /sdcard

if errorLevel 1 ( exit /b 2 )

adb -s %DEVICE_ID% shell twrp install /sdcard/%ARCHIVE_NAME%

if errorLevel 1 ( exit /b 3 )

timeout 1 >nul

adb -s %DEVICE_ID% shell rm /sdcard/%ARCHIVE_NAME%

timeout 1 >nul
