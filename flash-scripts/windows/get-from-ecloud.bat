:: Copyright (C) 2019-2020 ECORP SAS - Author: Romain Hunault, Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: %1: ARCHIVE_URL
:: %2: FOLDER (will be created if doesn't exist)

:: Exit status
:: - 0 : archive downloaded
:: - 1 : Generic error code (see wget exit status here: https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html)
:: - 2 : Parse error—for instance, when parsing command-line options, the ‘.wgetrc’ or ‘.netrc’... (see wget exit status here: https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html)
:: - 3 : File I/O error (see wget exit status here: https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html)
:: - 4 : Network failure (see wget exit status here: https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html)
:: - 5 : SSL verification failure (see wget exit status here: https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html)
:: - 6 : Username/password authentication failure (see wget exit status here: https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html)
:: - 7 : Protocol errors (see wget exit status here: https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html)
:: - 8 : Server issued an error response (see wget exit status here: https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html)
:: - 10: No internet connection
:: - 11: Invalid file downloaded
:: - 101 : ARCHIVE_URL missing
:: - 102 : FOLDER missing

set "ARCHIVE_URL=%1"
set "FOLDER=%2"

ping -n 1 gitlab.e.foundation 2>&1 > nul
if errorLevel 1 (
  exit /b 10
)
if not defined %FOLDER (
  exit /b 101
)
if not defined %ARCHIVE_URL (
  exit /b 102
)
mkdir %FOLDER%

:: I'm not certain of the below block
for %%a in ("%ARCHIVE_URL%") do (
	set "FILENAME=%%~na"
)

Powershell.exe -ExecutionPolicy remotesigned -Command "ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; Invoke-WebRequest -Uri '%ARCHIVE_URL%.sha256sum' -OutFile '%FOLDER%\%FILENAME%.sha256sum'"

:: the following two lines should be the last to transform into batch
:: FOLDER_ESCAPED=echo %FOLDER% | sed 's/\/$//' | sed 's/\//\\\//g'

:: this is a simple concatenation
:: sed -i 's/\(.*\)  \(.*\)/\1 '${FOLDER_ESCAPED}'\/\2/' ${FOLDER}/${FILENAME}.sha256sum

CertUtil -hashfile  %FOLDER%\%FILENAME%.sha256sum
if not errorLevel 1( 
  exit /b 0
)

curl -o %FOLDER%\%FILENAME% ARCHIVE_URL

CertUtil -hashfile %FOLDER%\%FILENAME%.sha256sum

if errorLevel 1 (
  rm ${FILENAME}
  exit /b 11
)
