/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

/**
 * Encapsulate data about the device
 * @author Vincent Bourgmayer
 */
public class Device {
    private String model;
    private String name;
    private String manufacturer;
    private String device;
    private String os;
    private String osApi;
    private String IMEI;
    private String adbId;
    
    public Device(String adbId){
        this.adbId = adbId;
    }
    
    public Device(String adbId, String model, String name, String manufacturer, String device, String IMEI, String os, String osApi){
        this.adbId = adbId;
        this.model = model;
        this.name = name;
        this.manufacturer = manufacturer;
        this.device = device;
        this.IMEI = IMEI;
        this.os = os;
        this.osApi = osApi;
    }

    
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getAdbId() {
        return adbId;
    }

    public void setAdbId(String adbId) {
        this.adbId = adbId;
    }
}
