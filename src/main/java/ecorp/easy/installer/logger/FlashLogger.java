/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.logger;

import ecorp.easy.installer.AppConstants;
import ecorp.flash.lib.loggers.AbstractLogger;
import ecorp.flash.lib.utils.IFlashHandler;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import javafx.application.Platform;

/**
 *
 * @author vincent Bourgmayer
 */
public class FlashLogger extends AbstractLogger{

    IFlashHandler displayOutput;
    
    public FlashLogger(IFlashHandler output){
        super();
        this.displayOutput = output;
        this.logs = new StringBuilder();
    }
    
    @Override
    public void writeLine(String log) {
        this.logs.append(log);
        Platform.runLater( () -> {
            displayOutput.onLogToDisplayRecieved(log); 
        });
    }

    /**
     * Write all the log content into a file
     * @return The absolute path to the file or null if there is an error during writing
     */
    public String writeTofile() {
        System.out.println("FlashLogger.writeLogToFile()");
        //generate unique file name thanks to UUID:
        //https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/UUID.html#randomUUID()
        String fileNameToHash=UUID.randomUUID()+".log";
        String logFilePath = AppConstants.getWritableFolder()+fileNameToHash;
        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(logFilePath, true)))) {
            out.write(logs.toString());
        } catch (IOException e) {
            System.err.println(e);
            return null;
        }
        return logFilePath;
    }  
    
    @Override
    public void writeTofile(String logFilePath, String log) {
        super.writeTofile(logFilePath, log);
    }

    @Override
    public void write(String log) {
        super.write(log); //To change body of generated methods, choose Tools | Templates.
    }
}
