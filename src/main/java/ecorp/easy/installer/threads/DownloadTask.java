/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.threads;


import ecorp.easy.installer.AppConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.concurrent.Task;

/**
 * this class verify the checksum of a file and download it
 * @author vincent Bourgmayer
 */
public class DownloadTask extends Task<Boolean>{
    /**
     * Constante taille
     */
    private static final long[] CST_SIZE = {1024, 1024*1024, 1024*1024*1024, 1024*1024*1024*1024};
    /**
     * Constante unité
     */
    private static final String[] CST_UNITS = {"KB", "MB", "GB", "TB"};
    
    final private ResourceBundle i18n;
    final private String targetUrl;
    final private String fileName;
    
    /**
     * COnstruction of the download task
     * @param targetUrl the web path to the resource
     * @param fileName name of the file
     * @param resources used to send already translated message
     */
    public DownloadTask(String targetUrl, String fileName, ResourceBundle resources){
        this.targetUrl = targetUrl;
        this.fileName = fileName;
        this.i18n = resources;
    }
    
    
    @Override
    protected Boolean call() throws Exception {
        final String localFilePath = AppConstants.getSourcesFolderPath()+fileName;
        final String checksumFilePath = localFilePath+".sha256sum";
        
        if(isCancelled()) return false;
        
        this.updateTitle("Downloading "+fileName+".sha256sum");
        
        // Download checksum. If file not downloaded return false and stop downloadin because integrity isn't guaranteed
        if( !downloadFile(targetUrl+".sha256sum", checksumFilePath) ){
            this.updateMessage(i18n.getString("download_lbl_cantcheckIntegrity"));
            return false;
        }
        
        //If checksum valid it means that file is already there and up to date
        if ( validChecksum(checksumFilePath) ){
             this.updateMessage(i18n.getString("download_lbl_fileAlreadyUptoDate"));
            return true;
        }

        if(isCancelled()) return false;
        
        this.updateTitle("Downloading "+fileName);

        
        if ( downloadFile(targetUrl, localFilePath) )//Download file
        {
            return validChecksum(checksumFilePath);
        }else{
            this.updateMessage(i18n.getString("download_lbl_downloadError"));
            return false;
        }
    }
    
    
    // method link to file downloading
    
    /**
     * Perform the downloading of the specified file
     * @return boolean true if file has been successfully downloaded, false either
     */
    private boolean downloadFile(final String fileURL, final String localFilePath) throws  MalformedURLException, IOException, InterruptedException{

        HttpURLConnection connect = (HttpURLConnection) new URL(fileURL).openConnection();
        connect.setReadTimeout(30000);
        connect.setConnectTimeout(30000);
        
        if(connect.getResponseCode() != HttpURLConnection.HTTP_OK){ //return false it resources is unreachable
            return false;
        }

        final double fileSize = connect.getContentLengthLong();
        final String formattedFileSize = formatFileSize(fileSize); //used for UI
        
        //Update UI
        updateProgress(-1, fileSize);
        updateMessage(formatFileSize(0.0)+" / "+formattedFileSize );
        
        boolean downloaded = false;

        //Download the file
        try(FileOutputStream fos = new FileOutputStream(localFilePath);
            InputStream is = connect.getInputStream();
            ReadableByteChannel rbc = Channels.newChannel(connect.getInputStream()); 
                
        ){
            TimeOutRunnable timeoutRunnable = new TimeOutRunnable();
            Thread timeoutThread = new Thread(timeoutRunnable);
            timeoutThread.start();

            long downloadAmount =0;
            long precedentAmount = 0;
            
            while ( rbc.isOpen() && !isCancelled() && ! downloaded ){
                //long d = downloadAmount + 1<<24;
                precedentAmount = downloadAmount;
                downloadAmount += fos.getChannel().transferFrom(rbc,downloadAmount,1 << 18);
                
                if(precedentAmount >= downloadAmount){
                    downloaded = false;
                    rbc.close();
                    connect.disconnect();
                }else{
                   timeoutRunnable.amountIncreased();

                    updateProgress(downloadAmount, fileSize);
                    updateMessage( formatFileSize(downloadAmount)+" / "+formattedFileSize);
                
                    fos.flush();
                    downloaded = (downloadAmount == fileSize);
                }
            }
            //end of download, stop the timeout thread
            timeoutRunnable.stop();
            timeoutThread.join(1000);
        }

        //If cancelled or not download, delete the partially downloaded file
        if(isCancelled() || ! downloaded){
            new File(localFilePath).delete();
            return false;
        }
        
        return downloaded;
    }
    
    /**
     * Format file size to use correct size name (mb, gb, ...)
     * @param value the file size formatted witht the good size catégorie
     * @return 
     */
    public String formatFileSize(final double value){
        double size;
        for (int i = 0; i < 3; i++) {
            size=value/CST_SIZE[i];
            if (size <= 1024) {
                return new DecimalFormat("#,##0.#").format(size) + " " + CST_UNITS[i] ;
            }
        }
        return null;
    }
    
    
    //Method about file checking
    
    /**
     * read the content of the checksum file
     * @param fileChecksum file containing checksum
     * @return null if no content. else a line in following format: checksum relativefilePath
     */
    private String readChecksumFile(String fileChecksum) throws IOException{
        Scanner sc = new Scanner(new FileReader(fileChecksum)); 
        if(sc.hasNextLine()){
          return sc.nextLine();
        }
        return null;
    }

    /**
     * Verify the integrity of the downloaded file
     * source: http://www.sha1-online.com/sha256-java/
     * @param fileChecksum
     * @return true if integrity has been validated
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    private boolean validChecksum( String fileChecksum) throws NoSuchAlgorithmException, IOException{
        System.out.println("ValidChecksum("+fileChecksum+")");
        updateMessage(i18n.getString("download_lbl_checkingIntegrity"));
        //get file containing checksum
        File sumFile = new File(fileChecksum);
        
        //If checksum file doesn't exist we can't validate checksum
        if( !sumFile.exists()){
            updateMessage(i18n.getString("download_lbl_cantcheckIntegrity"));
            return false;
        }
        
        //read content of file containing checksum to extract hash and filename
        System.out.println("fileChecksum: "+fileChecksum);
        String checksumLine = readChecksumFile(fileChecksum);
        System.out.println("ChecksumLine : "+checksumLine);
        String[] splittedLine = checksumLine.split("\\s+");

        //get file of the hash
        //if file concerned by checksum doesn't exist we can't validate
        File file = new File(AppConstants.getSourcesFolderPath()+splittedLine[1]);
        if(!file.exists()){
            updateMessage(i18n.getString("download_lbl_localFileNotFound"));
            System.out.println(splittedLine[1]+" do not exists");
            return false;
        }
        
        //Calcul fileHash of the localFile
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        FileInputStream fis = new FileInputStream(file);
  
        byte[] data = new byte[1024];
        int read = 0;
        updateProgress(-1,1);
        while ((read = fis.read(data)) != -1) { sha256.update(data, 0, read); }
        byte[] hashBytes = sha256.digest();
  
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashBytes.length; i++) {
          sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
         
        String fileHash = sb.toString();
        
        System.out.println(fileHash+" vs "+splittedLine[0]);
        return fileHash.equals(splittedLine[0]);
    }
    
    /**
     * Private inner class used to set a timeout on File's download
     */
    private class TimeOutRunnable implements Runnable{

        final long timeout = 10000; //10secondes
        long currentTime;
        boolean stop = false;

        synchronized void stop(){
            this.stop = true;
        }

        @Override
        public void run() {
            
            currentTime = System.currentTimeMillis();
            long previousTime;
            while(!stop){
                previousTime = currentTime;
                //isCancelled() is a method of the containing DownloadTask.java
                if(Thread.interrupted() || isCancelled() ) stop = true;
                try{
                    Thread.sleep(timeout); 
                    if(!stop && currentTime == previousTime){
                        System.out.println("No updates");
                        //updateProgress & updateMessage are methos of DownloadTask.java
                        updateProgress(-1, 1);
                        updateMessage(i18n.getString("download_lbl_connectionLost"));
                    }
                }catch(Exception e){
                    stop = true;
                    System.out.println("TimeoutThread crashed: "+e.toString());
                }
            }
            System.out.println("timeoutThread is over!");
        }
        //Signal that an amount was increased
        synchronized private void amountIncreased(){
            currentTime = System.currentTimeMillis();
        }
    };
            
}
