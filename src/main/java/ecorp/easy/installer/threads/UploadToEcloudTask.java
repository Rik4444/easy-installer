/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.threads;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Paths;
import java.util.Base64;
import javafx.concurrent.Task;

/**
 * The goal of this task is to push the specified file on ecloud folder
 * @author vincent Bourgmayer
 */
public class UploadToEcloudTask extends Task<Boolean>{
    String URL;
    String filePath;
    
    public UploadToEcloudTask(String URL, String filePath){
        this.URL = URL;
        this.filePath = filePath;
    }
    
    @Override
    protected Boolean call() throws Exception {
        File file = new File(filePath);
        if(!file.exists()) return false;
        
        String autorizationToken = encodeToBase64( URL.substring(URL.lastIndexOf("/")+1)+":" );
        
        var request = HttpRequest.newBuilder()
            .header("Referer", URL)
            .header("Authorization", "Basic "+autorizationToken) //the value is defined for the folder and is required! I've no idea how to automaticaly get it
            .uri(URI.create("https://ecloud.global/public.php/webdav/"+file.getName()))
            .PUT(HttpRequest.BodyPublishers.ofFile(Paths.get(filePath)))
            .build();
        

        var httpClient = HttpClient.newBuilder().followRedirects(HttpClient.Redirect.ALWAYS).build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println("Response code of log's upload : "+response.statusCode());
        if(response.statusCode()>= 200 && response.statusCode() < 300){
            file.delete();
            return true;
        }
        return false;
    }
    
    /**
     * Encode the given string into base64 String
     * @param string
     * @return
     * @throws UnsupportedEncodingException 
     */
    private String encodeToBase64(String string) throws UnsupportedEncodingException{        
        return  Base64.getEncoder().encodeToString( string.getBytes("UTF-8") );
    }
}
