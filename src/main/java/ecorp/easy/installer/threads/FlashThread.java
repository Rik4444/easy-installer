/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.threads;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.logger.FlashLogger;
import ecorp.easy.installer.models.Device;
import ecorp.easy.installer.models.StepUi;
import javafx.application.Platform;
import ecorp.flash.lib.models.Command;
import ecorp.flash.lib.threads.AbstractThread;
import ecorp.flash.lib.utils.Constants;
import ecorp.flash.lib.utils.DataBundle;
import ecorp.flash.lib.utils.IFlashHandler;


/**
 *
 * @author Vincent Bourgmayer
 */
public class FlashThread extends AbstractThread implements EasilyCancelable{
    boolean running = false;
    boolean cancelled = false;
    boolean sendIssueLog = false; //if true then the app must write the file for issue. @TODO replace the word "send" by a word more accurate.
    boolean sendRecordLog = true;
    boolean success = false; //used to run script one after one only if success @TODO check is still usefull
    boolean atLeastOneError = false; //Say if there has been at least one issue during all the process
    final IFlashHandler application;
    final Object pauseLock;
    final Device device;
    
    /**
     * Constructor for flashThread
     * @param controller This is the controller which change the UI depending on the flash thread result
     * @param firstCommandKey The command key to use to get the first step to perform
     * @param pauseLock a semaphor to stop allow to pause and restart the thread
     * @param device Object containing device info
     */
    public FlashThread(IFlashHandler controller, String firstCommandKey, Object pauseLock, Device device){
        super(firstCommandKey, new FlashLogger(controller));
        this.application = controller;
        this.pauseLock = pauseLock;
        this.device = device;
        String sourcePath = AppConstants.getSourcesFolderPath();

        //setParameters
        this.commonParameters.put("SOURCES_PATH", sourcePath);
        this.commonParameters.put("TWRP_IMAGE_PATH", sourcePath+Constants.getTwrpImgPath());
        this.commonParameters.put("ARCHIVE_PATH", sourcePath+Constants.getEArchivePath());
        this.commonParameters.put("ADB_FOLDER_PATH", AppConstants.getADBFolderPath());
        this.commonParameters.put("HEIMDALL_FOLDER_PATH", AppConstants.getHeimdallFolderPath());
        this.commonParameters.put("DEVICE_ID", device.getAdbId());
        this.commonParameters.put("DEVICE_DEVICE", device.getDevice());
    }

    /**
     * Handle result of a command
     * @param command
     * @return boolean true is success false either
     */
    private boolean handleResult(final Command command){
        System.out.println("FlashThread.handleResult()");
        //read result from command object
        final int exitValue = command.getExitValue();
        final String shellOutput = command.getShellOutput();
        
        this.logger.writeLine("Shell output: \n``` \n"+shellOutput);
        this.logger.writeLine("Exit value: "+exitValue+"\n```");

        if(command.isSuccess()){
            String outputKey = command.getOutputKey();
            //If an output is expected from succeed script
            if( outputKey != null && outputKey.charAt(0) == COMMON_PARAM_IDENTIFIER){
                final int lastIndex = shellOutput.lastIndexOf("\n");
                final int length = shellOutput.length();
                final String shellOutputCleaned = shellOutput.substring(lastIndex+1, length);
                System.out.println("shell output cleaned : "+shellOutputCleaned);
                this.commonParameters.put(outputKey.replace("${","").replace("}", ""), shellOutputCleaned);
            }
            
            return true;
        }else{ //fails case
            String errorMsgKey = command.getErrorMsg();
            if(errorMsgKey == null || errorMsgKey.isEmpty()){
                errorMsgKey = "script_error_unknown";
            }
            logger.write( "Exit value means: "+errorMsgKey );
            System.out.println(errorMsgKey);
            showError(errorMsgKey);
            return false;
        }
    }

    /**
     * send info to app to make it in error mode
     * @param errorMsgKey the error message translation key to send to UI
     */
    protected void showError(String errorMsgKey) {
        
        //@TODO remove below specific code
        final DataBundle bundle = new DataBundle();
        bundle.putString("errorMsgKey", errorMsgKey); //@Todo: put errorMsgKey as staticaly defined somewhere
        Platform.runLater(() -> {
            application.onFlashError(bundle);
        });
    }

    /**
     * Ask logger to create a log file and return its path
     * @return 
     */
    public String createLogFile(){        
        return ((FlashLogger) logger).writeTofile();
    }
    
    /**
     * stop the thread
     * @param sendLog if true, Logger will send data in logger
     */
    synchronized public void cancel(boolean sendLog){
        System.out.println("FlashThread.cancel()");
        if(running && commands.containsKey(currentStepCode)){ //@TODO the second part of the test won't work, as we include some "error" steps. So the commands.size isn't good.
            Command cmd = commands.get(currentStepCode);
            if(cmd != null) cmd.cancel();
        }
        else{ //if in success
            showError("flash_process_cancelled");
        }
        cancelled = true;
        if(!sendLog){
            sendIssueLog = false;
            sendRecordLog = false;
        }
    }

    
    /**
     * Finish the thread: send data to logger if needed, set running to false and try to start a new thread.
     */
    synchronized public void finish(){
        //if(sendIssueLog ) ((FlashLogger) logger).writeToFile("Flash");
        running = false;
        
        final DataBundle bundle = new DataBundle();
        bundle.putBoolean("atLeastOneError", atLeastOneError);
        bundle.putBoolean("sendRecordLog", sendRecordLog);
            
        Platform.runLater(() ->{
            application.onFlashThreadEnd(bundle);
        });
    }
    
    /**
     * @return true if thread is running else it return false 
     */
    synchronized public boolean isRunning(){
        return this.running;
    }
    

    @Override
    protected void doBeforeToRunCommand() {
        //Update UI
        final Command cmd = commands.get(currentStepCode);
        final DataBundle bundle = new DataBundle();
        if(cmd.getCommand().contains(AppConstants.ASK_ACCOUNT_KEY)){
            bundle.putString("stepType", AppConstants.ASK_ACCOUNT_KEY);
            //@TODO: Tell UI To update
            Platform.runLater(()->{
                    System.out.println("Update UI for"+cmd.getCommand());
                    application.onStepStart(bundle);
            });
            try{
                synchronized(pauseLock){
                    pauseLock.wait();
                }
            }catch (InterruptedException e) {} //Ignore the issue
            //@TODO Find a way to prevent end the run process of this command before to try to run a script
            //A posibility is to add a (cmd != null) ? cmd.execAndReadOutput : ; in AbsttractThread class and then here: se cmd to null.
        }else{
            //UpdateUI
            StepUi newUIValues = (StepUi) cmd.getNewUIValues();
            if(newUIValues != null) { 
                //@todo: Must define bundle key as a static String at a single place.
                bundle.putString("stepType", newUIValues.getType());
                bundle.putString("stepTitle", newUIValues.getTitle());
                bundle.putList("stepInstructions", String.class.getSimpleName(), newUIValues.getInstructions());
                bundle.putString("instructionImgName", newUIValues.getInstructionImgName());
                bundle.putString("stepType", newUIValues.getType());
                if(newUIValues.getType().equals("action")){
                    bundle.putString("titleIconName", newUIValues.getTitleIconName());
                }
                bundle.putInteger("averageTime", newUIValues.getAverageTime());
                Platform.runLater(()->{
                    System.out.println("Update UI for"+cmd.getCommand());
                    application.onStepStart(bundle);
                });
            }
        }
    }

    @Override
    protected void doAfterToRunCommand() {
        atLeastOneError =  (atLeastOneError || !success || cancelled);
        sendIssueLog = (atLeastOneError && !cancelled);
    }
    

    @Override
    protected void onErrorRaised(Exception e) {
        showError("java_error_unknow");
        System.out.println("catched exception "+e.toString());
        //send error in log rather that in UI
        sendIssueLog = true;
        atLeastOneError = true;
        logger.writeLine("Java exception: "+ e.toString());
    }

    @Override
    protected void onRunEnd() {
        final boolean issueEncountered = atLeastOneError;
        final DataBundle bundle = new DataBundle();
        bundle.putBoolean("onFlashStop", (success && !cancelled && !issueEncountered));
        Platform.runLater(() ->{
            //Do not use raw success. Because if error declared after a step has succeed then it will be considered as a succes
             application.onFlashStop( bundle);
        });
    }

    @Override
    protected void runInitialization() {
        running = true;
    }
    
    /**
     * Call "cancel" from super
     * @return this.cancelled
     */
    @Override
    public boolean onCancelRequestRecieved(){
        this.cancel(false);
        return this.cancelled;
    }
    
    
    
    @Override
    public void run(){
        if(commands.isEmpty()) return;
        runInitialization();
        try{
            //execute scripts
            String nextCommandKey = firstCommandKey;
            while(nextCommandKey != null ){
                currentStepCode = nextCommandKey;
                final Command cmd = commands.get(nextCommandKey);
                
                updateParameters();
                //UpdateUI
                doBeforeToRunCommand();          
                if(! cmd.getCommand().contains(AppConstants.ASK_ACCOUNT_KEY)){
                    //write into logger
                    this.logger.write("\n\n### Command : "+cmd.getCommand());
                    //run command
                    cmd.execAndReadOutput();
                    success = handleResult(cmd);
                }else{
                    success = true;
                }
                
                doAfterToRunCommand();
                
                nextCommandKey = cmd.getNextCommandKey();
            }

        }catch(Exception e){
            onErrorRaised(e);
        }
        onRunEnd();
    }
    
    /**
     * Return the number of step of the full process
     * @return 
     */
    public int getCommandsSize(){
        return this.commands.size();
    }
}
