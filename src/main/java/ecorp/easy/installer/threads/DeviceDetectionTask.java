/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.threads;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.exceptions.TooManyDevicesException;
import ecorp.easy.installer.models.Device;
import ecorp.flash.lib.models.Command;
import javafx.concurrent.Task;

/**
 *
 * @author vincent Bourgmayer
 */
public class DeviceDetectionTask extends Task<Device>{
    final String CMD_adbDevices = "devices";    
    
    @Override
    protected Device call() throws Exception{
        Device detectedDevice = null;

        detectedDevice = runAdbDevicesCmd(AppConstants.getADBFolderPath()+"adb");

        if(detectedDevice != null){
            System.out.println("Device found");
        }
        return detectedDevice;
    }
    
    /**
     * Run "adb devices -l" command
     * @param baseCmdString
     * @return
     * @throws Exception 
     */
    private Device runAdbDevicesCmd(String baseCmdString) throws Exception{
        System.out.println("runADBDevicesCmd()");
        
        Command cmd = new Command(baseCmdString);
        cmd.addParameter("1", CMD_adbDevices);
        cmd.addParameter("2", "-l");
        cmd.execAndReadOutput();     
        

        Device detectedDevice = null;
        String[] outputs = cmd.getShellOutput().split("\\R");
        System.out.println("raw shell outputs: "+cmd.getShellOutput());
        if(outputs.length <=1){
            System.out.println("Waiting");
            Thread.sleep(2250);
            return null;
        }
        int counter =0;
        for(String s : outputs){
            Device deviceFound = checkAdbDevicesResult(s);
            if(deviceFound != null){
                detectedDevice = deviceFound;
                ++counter;
            }
        }
        if(counter > 1){
            System.out.println("Too many devices detected");
            throw new TooManyDevicesException(counter);
        }
        if(detectedDevice==null) {
            System.out.println("waiting");
            Thread.sleep(2250);
        }
        return detectedDevice;
    }
    
    
    
    /**
     * Analyse one line from result of "ADB devices -l" command
     * @param resultLine
     * @return the adb's device's id if found else it return empty string
     */
    private Device checkAdbDevicesResult(String resultLine){
        System.out.println("checkAdbDevicesResult("+resultLine+")");
        boolean deviceFound = false;
        Device result = null;
        
        //Split string on each space
        String[] datas = resultLine.split("\\s+");
        
        //loop over each subString
        for(String stringPart : datas){
            System.out.println("Current subString : "+stringPart);
            //Detect the line that says that device is found
            if(!stringPart.isEmpty() && (stringPart.endsWith("device") ||stringPart.endsWith("recovery") ) ){
                System.out.println("Device has been found");
                deviceFound = true;
                result = new Device(datas[0]); //get adb id
            }
            //Read data and store them in Device object
            if(deviceFound){
               if(stringPart.contains("product:")){
                   System.out.println("\"product\" keyword has been found");
                    result.setName(stringPart.substring("product:".length() ));
               }else if(stringPart.contains("model:")){
                   System.out.println("\"model\" keyword has been found");
                   result.setModel(stringPart.substring("model:".length() ));
               }else if(stringPart.contains("device:")){
                    System.out.println("\"device\" keyword has been found");
                   result.setDevice(stringPart.substring("device:".length() ));
               }
            }
        }
        return result;
    }
}
