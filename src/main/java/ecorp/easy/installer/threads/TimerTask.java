/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.threads;
import ecorp.easy.installer.controllers.subcontrollers.FlashSceneController;
import javafx.application.Platform;
import javafx.concurrent.Task;

/**
 *
 * @author vincent Bourgmayer
 */
public class TimerTask extends Task<Void>{

    double averageTime;
    FlashSceneController controller;
    
    /**
     * Constructor for TimerTask
     * @param averageTime time in seconds
     * @param controller FlashSceneController
     */
    public TimerTask(int averageTime, FlashSceneController controller){
        this.averageTime = averageTime*1000.0;
        this.controller = controller;
    }
    
    /**
     * Implementation of this method is required to extends Task.
     * 
     * @return because it extends Task< Void >
     * @throws Exception 
     */
    @Override
    protected Void call() throws Exception {
        
        double startTime = System.currentTimeMillis();
        double elapsedTime = 0;
        Platform.runLater(() -> {
                controller.updateProgressIndicator(0.0);
        });
        while(!isCancelled() &&  elapsedTime < averageTime){
            Thread.sleep(300); //try with 150
            double newTime = System.currentTimeMillis();
            elapsedTime = newTime - startTime;
            
            double percent = elapsedTime / averageTime; 
            if(percent  < 100.0) {
                Platform.runLater(() -> {
                    controller.updateProgressIndicator(percent);
                });
            }
        }
        return null;
    }
}
