/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.controllers.MainWindowController;
import ecorp.easy.installer.graphics.FlashGlobalProgressManager;
import ecorp.easy.installer.threads.EasilyCancelable;
import ecorp.easy.installer.threads.FlashThread;
import ecorp.easy.installer.threads.TimerTask;
import ecorp.easy.installer.threads.UploadToEcloudTask;
import ecorp.flash.lib.utils.DataBundle;
import ecorp.flash.lib.utils.IFlashHandler;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;

/**
 * FXML Controller class
 *
 * @author Vincent Bourgmayer
 */
public class FlashSceneController extends AbstractSubSteppedController implements IFlashHandler {
    //Current step UI element
    @FXML VBox flashSceneRoot;
    @FXML ProgressBar loadStepProgressIndicator; //'load' step type only
    @FXML TextFlow instructionsFlow;
    @FXML Label stepTitleLabel;
    @FXML ImageView instructionImage;
    @FXML ImageView stepTitleIcon;

    // progress bar's node of global Flash's process :
    @FXML HBox globalProgressIndicator;
    private FlashGlobalProgressManager globalProgressMgr;
    
    // node relative to Log
    @FXML VBox instructionsVBox;
    @FXML ScrollPane logScrollPane;
    @FXML TextFlow logFlow;
    @FXML Button showHideLogBtn;
    @FXML Button sendLogBtn;
    
    private TimerTask timer; //This is the task which make progression in a progress bar
    private boolean stopped = false; //determine if the process has been stopped (due to error or normal ending)
    private List<String> instructionsKey; //Contains the different instruction that have to be currently displayed
    ResourceBundle instructionsImagesBundle; //Give access to image to loads with instructionsKey
    private FlashThread thread;
    private Object pauseLock; //lock used for pausing the FlashThread

    
    
    @Override    
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        
        sendLogBtn.managedProperty().bind(sendLogBtn.visibleProperty());
        loadStepProgressIndicator.managedProperty().bind(loadStepProgressIndicator.visibleProperty());
        logScrollPane.managedProperty().bind(logScrollPane.visibleProperty());
        logFlow.heightProperty().addListener(observable -> logScrollPane.setVvalue(1.0));   
        
        logScrollPane.setVisible(false);
        sendLogBtn.setVisible(false); 

         //can use Properties instead of ResourceBundle but ResourceBundle use default file for missing value
        //The only problem is that I can't specify any version without region and language...
        instructionsImagesBundle = ResourceBundle.getBundle("instructions.imageName", new Locale.Builder().setRegion("en").setLanguage("EN").setVariant(AppConstants.DEVICE_MODEL).build());
        
    }
    
    @Override
    public void setParentController(MainWindowController parentController) {
        super.setParentController(parentController); //To change body of generated methods, choose Tools | Templates.
        pauseLock = new Object();

        thread = parentController.getThreadFactory().buildFlashThread(this, pauseLock);
        if(thread!= null){
            parentController.setThread((EasilyCancelable) thread);
           
            //@TODO find another way to get the step number as yaml file can contains step for issue. By example
            //if script got issue, then load script 45 if success load script 2.
            //Then the stepSize won't fit with the real step that the user must perform
            globalProgressMgr = new FlashGlobalProgressManager(thread.getCommandsSize());
            globalProgressIndicator.getChildren().addAll(globalProgressMgr.getSegments());
            
            thread.start();
        } 

    }
    
    /**
     * Add a new instruction to display without clearing the precedent
     * @param instruction the instruction to add
     */
    public void addInstructionToDisplay(String instruction){
        Label label = new Label(i18n.getString(instruction));
        label.setWrapText(true); //make text resize if longer than width
        label.minWidthProperty().bind(instructionsFlow.widthProperty()); //avoid to display two instruction on the same line
        label.maxWidthProperty().bind(instructionsFlow.widthProperty()); //Set max Width to its parent width
        instructionsFlow.getChildren().add(label);
    }
    
    /**
     * Change color of the block of the progressBar
     */
    private void updateProgressBar(){
        globalProgressMgr.updateProgression();
    }
    

    
    /**
     * UI changes at beginning of a new step
     * @TODO: move part of code relative to 'action' step to 'setActionStepUI()', and 'load' type to 'setLoadStepUI()'
     * @param db 
     */
    @Override
    public void onStepStart(DataBundle db) {
        System.out.println("onStepStart");
        if(db == null)return;
        
        if(timer != null){
            timer.cancel();
            timer = null;
        }

        //Update the progressBar of the global process
        updateProgressBar();
              
        //update instructions
        instructionsKey = db.getList("stepInstructions", "String");
        if(instructionsKey != null){
            instructionsFlow.getChildren().clear();
            instructionsKey.forEach((instruction) -> {
                addInstructionToDisplay(instruction);
            });
            
            //Update instruction image
            String instructionImgName = instructionsKey.get(0);
            try{
                this.instructionImage.setImage(new Image(getClass().getResourceAsStream("/images/"+instructionsImagesBundle.getString(instructionImgName))));
            }catch (Exception e) {
                this.instructionImage.setImage(null);
            }
        }
        
        //Update instruction title
        String title = db.getString("stepTitle");
        if(title != null){
            stepTitleLabel.setText(i18n.getString(title));
        }

        //Get Type of step ("action" or "load")
        String stepType = db.getString("stepType");
        if(stepType.equals("action") ){ onActionStepType(db); }
        else if( stepType.equals("load") ) { onLoadStepType(db); }
        else if (stepType.equals(AppConstants.ASK_ACCOUNT_KEY) ) { 
            displayEAccountView();
        }
    }

    /**
     * Display the specific interface for
     * Note: This could be replace by a call to parentController.loadSubUI("6-2-eAccount.fxml")
     * but how to reset current ui to this one after ?
     */
    private void displayEAccountView(){
        //Masque previous UI
        //@TODO it's better to apply to each children
        flashSceneRoot.setVisible(false);
        flashSceneRoot.setManaged(false);
        //Affiche the new One
        final String tempRootId = parentController.loadSubUI("6-2-eAccount.fxml");
        parentController.setNextButtonOnClickListener(( MouseEvent event) -> {
            parentController.resetNextButtonEventHandler();
            parentController.removeNodeFromRoot(tempRootId);
            parentController.showCurrentSubRoot();
            parentController.setNextButtonVisible(false);
            synchronized (pauseLock) {
                pauseLock.notify();
            }

        });
    }

    /**
     * Set UI for "load" steps
     * @param db 
     */
    private void onLoadStepType(DataBundle db){
        instructionsVBox.setAlignment(Pos.CENTER);
        parentController.setNextButtonVisible(false);
        int averageTime = db.getInt("averageTime");
        System.out.println("averageTime is: "+averageTime);
        if(averageTime > -1){ //Or > 0 ?
            timer = new TimerTask(averageTime, this);
            loadStepProgressIndicator.setProgress(0.0); //Reset to prevent showing last value
            loadStepProgressIndicator.setVisible(true);
            new Thread(timer).start();
        }
        this.stepTitleIcon.setImage(null);
        this.instructionsFlow.setVisible(false); //put it invisible instead ?
    }
    
    /**
     * Set UI for "action" steps.
     * @param db 
     */
    private void onActionStepType(DataBundle db){
        //emphasize first Label
        instructionsVBox.setAlignment(Pos.TOP_LEFT);
        currentSubStepId = 0; //reset the value
        emphasizeLabel((Label)instructionsFlow.getChildren().get(0));
        parentController.setNextButtonVisible(true);
        // /!\ LINE BELOW IS TEMPORARY
        setNextButtonOnClickListener();

        //Hide progress Indicator
        loadStepProgressIndicator.setVisible(false);
        instructionsFlow.setVisible(true);

        //Set Title Icon if defined
        String titleIconName = db.getString("titleIconName");
        if(titleIconName != null)
            this.stepTitleIcon.setImage(new Image(getClass().getResourceAsStream("/images/"+titleIconName)));
    }
    
    
    /**
     * UI modification when an error is detected during the process
     * @param db bundle that contains element to update the UI. Currently not used
     */
    @Override
    public void onFlashError(DataBundle db) {
        System.out.println("onFlashError");
        
        String errorMsgKey = db.getString("errorMsgKey");
        if(errorMsgKey == null || errorMsgKey.isEmpty())
            errorMsgKey ="script_error_unknown";
        stepTitleLabel.setText(i18n.getString(errorMsgKey));
        
        instructionsFlow.setManaged(false);
        instructionsFlow.setVisible(false);
        sendLogBtn.setVisible(true);
        
        Button tryAgainBtn = new Button(i18n.getString("all_lbl_tryAgain"));
        tryAgainBtn.setOnMouseClicked(( MouseEvent event) -> {
            parentController.retryToFlash();
        });
        instructionsVBox.setAlignment(Pos.TOP_CENTER);
        instructionsVBox.getChildren().add(tryAgainBtn);
    }

    /**
     * UI modification when a FlashThread end
     * @param db bundle containing element to update the UI. Currently not used
     */
    @Override
    public void onFlashThreadEnd(DataBundle db) {
        System.out.println("onFlashThreadEnd");
        stepTitleLabel.setText("FlashEnd");
    }

    /**
     * Behaviour of the UI when the flash process is stop. It also cancel the TimerTask of 'load' type task.
     * @TODO: check if it's not the same as 'onFlashThreadEnd' and how to clearly make difference. 
     * @param db bundle containing element to update the UI.
     */
    @Override
    public void onFlashStop(DataBundle db) {
        System.out.println("onFlashStop");
        //flashStepTitleLabel.setText("Stopped");
        if(timer != null){
            timer.cancel();
            timer = null;
        }
        stopped = true;
        parentController.resetNextButtonEventHandler();
        parentController.setNextButtonVisible(true);
        if(db.getBoolean("onFlashStop")){
            parentController.loadSubScene();
        }
    }
    

    /**
     * Add a log message to the log textFlow
     * todo: do it really need to be synchronized ?
     * @param string the log to display
     */
    @Override
    public synchronized void onLogToDisplayRecieved(String string) {
        Label label = new Label(string);
        label.setWrapText(true);
        label.setStyle("-fx-font-size:15px;");
        logFlow.getChildren().add(label);
    }
    
    /**
     * enable/disable log displaying
     */
    public void showHideLog(){
        String text = showHideLogBtn.getText();
        if(text.equals(">")){
            showHideLogBtn.setText("V");
            logScrollPane.setVisible(false);
        }else{
            showHideLogBtn.setText(">");
            logScrollPane.setVisible(true);
        }
    }
    
    /**
     * Send log to support, in special ecloud.global folder
     * @return 
     */
    public boolean sendLogToSupport(){
        if(thread != null){
            String filePath = thread.createLogFile();
            if(filePath != null){

                UploadToEcloudTask uploadTask = new UploadToEcloudTask(AppConstants.LOG_STORAGE_URL, filePath);
                uploadTask.setOnSucceeded(eh -> {
                    if( (Boolean) eh.getSource().getValue() ){ //if success
                        sendLogBtn.setDisable(true);
                        sendLogBtn.setText(i18n.getString("install_btn_sendLogSuccess"));
                        System.out.println("sending log: success");
                    }
                    else{
                        sendLogBtn.setText(i18n.getString("install_btn_sendLogAgain")); 
                        System.out.println("sending log: failed");
                    }
                });
                
                uploadTask.setOnFailed(eh->{
                    sendLogBtn.setText(i18n.getString("install_btn_sendLogAgain")); 
                    System.out.println("sending log error: "+eh.getSource().getException().toString());
                });
                
                new Thread(uploadTask).start();
            }
        }
        return false;
    }
    
    
    /**
     * Update the 'load' step's progress indicator.
     * It is called by the TimeTask
     * @param progression 
     */
    public synchronized void updateProgressIndicator(double progression){
        loadStepProgressIndicator.setProgress(progression);
    }

    /**
     * The new behaviour of the parentController's nextButton
     */
    @Override
    protected void onNextButtonClicked() {
        //Si currenttype est action
        final ObservableList children = instructionsFlow.getChildren();
        final int instructionsNumber = children.size();
        //get CurrentLabel 
        Label currentSubStepLabel = (Label) children.get(currentSubStepId);
        //get previousLabel
        Label nextSubStepLabel = (Label) children.get(currentSubStepId+1);     
        
        //Update the image using instruction key code
        //Note: I tried using Java 9 Stream API with filter, ... but code is longer
        //so I got back to simple for loop
        for(String instruction:instructionsKey){
            if(i18n.getString(instruction).equals(nextSubStepLabel.getText() ) ){
                Image img;
                try{
                    img = new Image(getClass().getResourceAsStream("/images/"+instructionsImagesBundle.getString(instruction)));
                }catch(Exception e){ img = null; }
                
                instructionImage.setImage(img);
                break;               
            }
        }
        
        if(currentSubStepId == instructionsNumber-2 && !stopped){
            parentController.setNextButtonVisible(false); //TO prevent clicking before the end of the full process
        }

        if(currentSubStepId < instructionsNumber-1){
            deemphasizeLabel(currentSubStepLabel);
            emphasizeLabel(nextSubStepLabel);
        }
    }
    
    /**
     * Need to implement this because it is a method define in IFlashHandler interface.
     * @TODO Must check where and when it used. If not, I must remove it from IFlashHandler and then from this controller.
     * @param string
     * @param db 
     */
    @Override
    public void onEvent(String string, DataBundle db) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
