/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import ecorp.easy.installer.controllers.MainWindowController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

/**
 *
 * @author Vincent Bourgmayer
 */
public abstract class AbstractSubController implements Initializable{
    protected MainWindowController parentController;
    protected ResourceBundle i18n;
    /**
     * Set the mainWindow controller as the root controller
     *   improvement: Replacement of MainWindowController class by an interface would be a good improvement
     *   for reduce complexity
     * @param parentController MainWindowController
     */
    public void setParentController(MainWindowController parentController){
        this.parentController = parentController;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        i18n = resources;
        //DO nothing
    }

}
