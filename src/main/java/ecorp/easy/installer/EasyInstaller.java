/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer;


import static ecorp.easy.installer.AppConstants.JavaHome;
import static ecorp.easy.installer.AppConstants.OsName;
import ecorp.easy.installer.controllers.MainWindowController;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Vincent Bourgmayer
 */
public class EasyInstaller extends Application {
    public final static String FXML_PATH ="/fxml/";
    private ResourceBundle i18n; //i18n mean "internationalization"

    private MainWindowController controller;
    
    @Override
    public void start(Stage stage) throws Exception {
        //four lines below are just for test
        System.out.println("OS name: "+OsName);
        System.out.println("Java Home: "+JavaHome);
        System.out.println("Current working dir: "+Paths.get("").toAbsolutePath().toString());
        System.out.println("ADB folder path: "+AppConstants.getADBFolderPath() );
        
        Locale currentLocale= Locale.getDefault();

        i18n =  ResourceBundle.getBundle("lang.translation", currentLocale);
        
       //Load main view
        FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_PATH+"mainWindow.fxml"));
        loader.setResources(i18n);
        Parent root = loader.load() ;
        controller = loader.getController();
        
        //Defines some properties
        Scene scene = new Scene(root);
        stage.setTitle(i18n.getString("appTitle")+"v0.8.3-beta");
        stage.setScene(scene);

        stage.setResizable(true); 
        stage.minHeightProperty().setValue(768);
        stage.minWidthProperty().setValue(1024.0);
        Font.loadFonts(getClass().getResource("/fonts/ufonts.com_century-gothic.ttf").toExternalForm(),20);
        
        double screenWidth= Screen.getPrimary().getVisualBounds().getWidth();
        double screenHeight = Screen.getPrimary().getVisualBounds().getHeight();
        
        if(screenWidth >= 1440.0 && screenHeight >= 1024.0){
            stage.setWidth(1440);
            stage.setHeight(1024);
        }else{
            stage.setWidth(screenWidth);
            stage.setHeight(screenHeight);
        }
        //Display UI
        stage.show();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    
    @Override
    public void stop(){
        System.out.println("Stage is closing");
        // Save file
        controller.onStop();
    }
}
